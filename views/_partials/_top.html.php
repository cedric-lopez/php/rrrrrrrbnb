<!doctype html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/styles/style.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,400;0,600;1,700&family=Nanum+Brush+Script&display=swap" rel="stylesheet">
	<title><?php echo $title_tag ?> - RRRrrrrBnB</title>
</head>

<body>
	<header>
		<div class="container-left">
			<nav>
				<div class="logo">RRRrrrrBnB</div>

				<a href="/">Annonces</a>
				<a href="/reservations">Réservations</a>
				<?php echo $is_owner ? '<a href="/nouvelle-annonce">Ajouter une annonce</a>' : '' ?>


			</nav>
		</div>
		<div class="container-right">
			<div class="auth">

				<a href="/login/logout">Deconnection</a>
			</div>


		</div>
	</header>

	<main>