<div class="pad"></div>
<div class="container">
	<h1><?php echo $h1_tag ?></h1>


	<ul>
		<div class="grid">
			<?php foreach ($bookings as $booking) : ?>
				<li>


					<p>date d'arrivée: <?php echo $booking->check_in ?></p>
					<p>date de départ: <?php echo $booking->check_out ?></p>
					<div class="card">

						<h2><?php echo $booking->rental->title ?></h2>
						<p class="infos"><?php echo $booking->rental->GetType()  . ' / ' . $booking->rental->capacity ?> pers</p>
						<p><?php echo $booking->rental->address->country ?></p>
						<p><?php echo $booking->rental->address->city ?></p>



					</div>
				</li>
			<?php endforeach ?>
		</div>
	</ul>
</div>