<div class="hero">
	<div class="hero-left">
		<p class="hero-title">Trouvez l’habitat
			dont vous avez
			toujours rêvé</p>
		<p>Cavernes, huttes, abris. Tout ce dont vous avez besoins pour accueillir toute votre tribu à l’abri de tout environement hostile.</p>
		<img src="/assets/img/arrow.png" alt="">
	</div>
	<div class="hero-right"></div>
</div>
<section>
	<div class="container">

		<h1><?php echo $h1_tag ?></h1>


		<ul>
			<div class="grid">
				<?php foreach ($rentals as $rental) : ?>
					<div class="card">
						<li>
							<div class="img">
								<img src="/assets/upload/<?php echo $rental->id . $rental->image ?>" alt="">
							</div>
							<div class="price"><?php echo $rental->price ?> </div>
							<h2><?php echo $rental->title ?></h2>
							<p class="infos"><?php echo $rental->GetType() . ' / ' . $rental->capacity ?> pers</p>
							<p><?php echo $rental->address->country ?></p>
							<p><?php echo $rental->address->city ?></p>



							<?php echo !$is_owner ? '<a href="/details/' . $rental->id . '">details</a>' : '' ?>
					</div>
					</li>
				<?php endforeach ?>
			</div>
		</ul>

	</div>
</section>