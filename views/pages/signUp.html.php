<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/styles/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,400;0,600;1,700&family=Nanum+Brush+Script&display=swap" rel="stylesheet">
    <title><?php echo $title_tag ?> - RRRrrrrBnB</title>

</head>

<body>
    <div class="container">
        <div class="logo">RRRrrrrBnB</div>

        <h1><?php echo $h1_tag ?></h1>
        <?php echo $res ?>
        <form action="/login" method="post">

            <div>
                <label for="mail">e-mail&nbsp;:</label>
                <input type="email" id="mail" name="email">
            </div>
            <div>
                <label for="pass">Mot de passe</label>
                <input type="password" id="pass" name="pass">
            </div>

            <div>
                <input type="checkbox" id="owner" name="owner">
                <label for="owner">Je suis annonceur</label>
            </div>
            <p><input type="submit" value="OK"></p>
        </form>
        <a href="/login">Connection</a>
    </div>
</body>

</html>