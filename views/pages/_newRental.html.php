<div class="pad"></div>
<div class="container">
	<h1><?php echo $h1_tag ?></h1>
	<?php echo $res ?>
	<form action="" method="post" enctype="multipart/form-data">
		<p><label for="title">Titre:</label>
			<input type="text" name="title" id="title" />
		</p>
		<p><label for="surface">Surface:</label>
			<input type="text" name="surface" id="surface" />
		</p>
		<p><?php echo isset($errors['surface']) ? $errors['surface'] : '' ?></p>

		<p><label for="description">Description:</label>
			<textarea id="description" name="description" rows="5" cols="33">

</textarea>
		</p>
		<p><label for="capacity">Capacité:</label>
			<input type="text" name="capacity" id="capacity" />
		</p>
		<p><?php echo isset($errors['capacity']) ? $errors['capacity'] : '' ?></p>
		<p><label for="price">Prix:</label>
			<input type="text" name="price" id="price" />
		</p>
		<p><?php echo isset($errors['price']) ? $errors['price'] : '' ?></p>

		<?php ?>

		<label for="type-select">Type de logement:</label>

		<select name="type" id="type-select">
			<option value="">--Veuillez selectionner un type--</option>


			<?php foreach ($types as $type => $value) : ?>

				<option value=<?php echo $value ?>><?php echo $type ?></option>

			<?php endforeach ?>



		</select>


		<p><label for="country">Pays:</label>
			<input type="text" name="country" id="country" />
		</p>
		<p><label for="city">Ville:</label>
			<input type="text" name="city" id="city" />
		</p>

		<fieldset>
			<legend>Equipments:</legend>

			<?php foreach ($equipments as $equipment) : ?>

				<div>
					<input type="checkbox" id="<?php echo $equipment->name  ?>" name="equipments[]" value="<?php echo $equipment->id  ?>">
					<label for="equipments"><?php echo $equipment->name  ?></label>
				</div>
			<?php endforeach ?>
		</fieldset>
		<label for="file">Ajouter une image</label>
		<input type="file" name="file">
		<p><input type="submit" value="OK"></p>
	</form>
</div>