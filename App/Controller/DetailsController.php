<?php

namespace App\Controller;


use App\AppRepoManager;
use LidemCore\View;

class DetailsController
{
	public function details(int $id): void
	{

		$get_rental = AppRepoManager::getRm()->getRentalRepo()->findById($id);

		$view_data = [
			'h1_tag' => $get_rental->title . ' - Détails',
			'rental' => $get_rental,
			'is_owner' => $_SESSION['type']
		];

		$view = new View('pages/details');
		$view->title = $get_rental->title;
		$view->render($view_data);
	}
	public function newBooking(int $id): void
	{
		$booking = AppRepoManager::getRm()->getBookingsRepo()->addBooking($id);
		$view_data = [
			'h1_tag' => 'Réservation',
			'is_owner' => $_SESSION['type'],
			'res' => $booking
		];

		$view = new View('pages/booking');
		$view->title = 'Réservations';
		$view->render($view_data);
	}
}
