<?php

namespace App\Controller;


use LidemCore\View;
use App\AppRepoManager;


class AuthController
{


	public function signUp(): void
	{

		$view_data = [
			'res' => '',
			'h1_tag' => 'inscription'

		];
		$view = new View('pages/signUp', true);
		$view->title = 'inscription';
		$view->render($view_data);
	}
	public function addUser(): void
	{
		$user = AppRepoManager::getRm()->getAuthRepo()->addUser();

		$view_data = [
			'res' => $user,
			'h1_tag' => 'connection'
		];
		$view = new View('pages/login', true);
		$view->title = 'connection';
		$view->render($view_data);
	}
	public function login(): void
	{

		$view_data = [
			'res' => '',
			'h1_tag' => 'connection'

		];
		$view = new View('pages/login', true);
		$view->title = 'connection';
		$view->render($view_data);
	}
	public function logout($logout): void
	{
		if (isset($logout)) session_destroy();
		$view_data = [
			'res' => '',
			'h1_tag' => 'connection'

		];
		$view = new View('pages/login', true);
		$view->title = 'connection';
		$view->render($view_data);
	}
}
