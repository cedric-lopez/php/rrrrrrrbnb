<?php

namespace App\Controller;


use App\AppRepoManager;
use LidemCore\View;

class BookingsController
{
	public function bookings(): void
	{
		$get_bookings = $_SESSION['type'] ? AppRepoManager::getRm()->getBookingsRepo()->findAllByOwner() : AppRepoManager::getRm()->getBookingsRepo()->findAll();

		$view_data = [
			'h1_tag' => 'Réservations',
			'bookings' => $get_bookings,
			'is_owner' => $_SESSION['type']
		];

		$view = new View('pages/bookings');
		$view->title = 'Réservations';
		$view->render($view_data);
	}
}
