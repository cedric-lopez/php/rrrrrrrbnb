<?php

namespace App\Controller;


use App\AppRepoManager;
use LidemCore\View;

class HomeController
{
	public function log(): void
	{
		!is_null(AppRepoManager::getRm()->getAuthRepo()->login()) ? $this->home() : 'error';
	}
	public function home(): void
	{
		$is_owner = $_SESSION['type'];
		$get_rentals = $is_owner ? AppRepoManager::getRm()->getRentalRepo()->findAllByOwner() : AppRepoManager::getRm()->getRentalRepo()->findAll();
		$view_data = [
			'h1_tag' => 'Annonces',
			'rentals' => $get_rentals,
			'is_owner' => $is_owner
		];

		$view = new View('pages/home');
		$view->title = 'Annonces';
		$view->render($view_data);
	}
}
