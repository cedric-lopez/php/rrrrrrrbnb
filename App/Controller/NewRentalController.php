<?php

namespace App\Controller;

use App\App;
use App\AppRepoManager;
use LidemCore\View;
use App\Model\Rental;


class NewRentalController
{
	public string $message;
	public array $errors = [];
	public function newRental(): void
	{

		$equipments = AppRepoManager::getRm()->getEquipmentRepo()->findAll();
		$view_data = [
			'h1_tag' => 'Nouvelle Annonce',
			'is_owner' => $_SESSION['type'],
			'types' => ['Abris' => Rental::TYPE_ABRIS, 'Caverne' => Rental::TYPE_CAVERNE, 'Hutte' => Rental::TYPE_HUTTE],
			'res' => '',
			'errors' => $this->errors,
			'equipments' => $equipments
		];

		$view = new View('pages/newRental');
		$view->title = 'Réservations';
		$view->render($view_data);
	}
	public function addRental(): void
	{


		$tmp_name = $_FILES['file']['tmp_name'];
		$name = trim($_FILES['file']['name']);
		$this->verif() ? $this->message = AppRepoManager::getRm()->getRentalRepo()->addRental($tmp_name, $name) : $this->message;

		$equipments = AppRepoManager::getRm()->getEquipmentRepo()->findAll();
		$view_data = [
			'h1_tag' => 'Nouvelle Annonce',
			'is_owner' => $_SESSION['type'],
			'types' => ['Abris' => Rental::TYPE_ABRIS, 'Caverne' => Rental::TYPE_CAVERNE, 'Hutte' => Rental::TYPE_HUTTE],
			'res' => $this->message,
			'errors' => $this->errors,
			'equipments' => $equipments
		];

		$view = new View('pages/newRental');
		$view->title = 'Réservations';
		$view->render($view_data);
	}
	private function verif(): bool

	{
		$verif = true;
		foreach ($_POST as $input) {
			if (empty($input)) {
				$this->message = 'veuillez remplir tous les champs';
				return false;
			}
		}

		if (intval($_POST['surface']) === 0) {
			$this->message = 'valeurs incorrectes';
			$this->errors['surface'] = 'Veuillez saisir un nombre';
			$verif = false;
		}
		if (intval($_POST['capacity']) === 0) {
			$this->message = 'valeurs incorrectes';
			$this->errors['capacity'] = 'Veuillez saisir un nombre';
			$verif = false;
		}
		if (intval($_POST['price']) === 0) {
			$this->message = 'valeurs incorrectes';
			$this->errors['price'] = 'Veuillez saisir un nombre';
			$verif = false;
		}
		return $verif;
	}
}
