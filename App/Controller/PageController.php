<?php

namespace App\Controller;


use LidemCore\View;


class PageController
{


	public function legalNotice(): void
	{

		$view_data = [
			'h1_tag' => 'mentions legales',
			'is_owner' => $_SESSION['type']
		];
		$view = new View('pages/legal-notice');
		$view->title = 'Mentions illégalement illégales';
		$view->render($view_data);
	}
}
