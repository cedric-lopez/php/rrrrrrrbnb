<?php

namespace App;

use MiladRahimi\PhpRouter\Exceptions\InvalidCallableException;
use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
use MiladRahimi\PhpRouter\Router;

use App\Controller\PageController;
use App\Controller\HomeController;
use App\Controller\DetailsController;
use App\Controller\BookingsController;
use App\Controller\NewRentalController;
use App\Controller\AuthController;
use LidemCore\Database\DatabaseConfigInterface;
use LidemCore\View;
use App\Middleware\GuestMiddleware;
use App\Middleware\UserMiddleware;
use App\Middleware\OwnerMiddleware;






class App implements DatabaseConfigInterface
{

	private const DB_HOST = 'database';
	private const DB_NAME = 'lamp';
	private const DB_USER = 'lamp';
	private const DB_PASS = 'lamp';

	private static ?self $instance = null;
	public static function getApp(): self
	{
		if (is_null(self::$instance)) self::$instance = new self();

		return self::$instance;
	}

	private Router $router;

	private function __construct()
	{
		$this->router = Router::create();
	}

	public function getHost(): string
	{
		return self::DB_HOST;
	}

	public function getName(): string
	{
		return self::DB_NAME;
	}

	public function getUser(): string
	{
		return self::DB_USER;
	}

	public function getPass(): string
	{
		return self::DB_PASS;
	}

	public function start(): void
	{


		// Démarrage de la session
		session_start();

		$this->registerRoutes();
		$this->startRouter();
	}

	private function registerRoutes(): void
	{
		$this->router->pattern('id', '[1-9]\d*');
		$this->router->group(['middleware' => [GuestMiddleware::class]], function () {
			$this->router->get('/inscription', [AuthController::class, 'signUp']);
			$this->router->get('/login', [AuthController::class, 'login']);
			$this->router->post('/login', [AuthController::class, 'addUser']);
			$this->router->post('/', [HomeController::class, 'log']);
		});
		$this->router->group(['middleware' => [UserMiddleware::class]], function () {

			$this->router->get('/', [HomeController::class, 'home']);

			$this->router->get('/mentions-legales', [PageController::class, 'legalNotice']);
			$this->router->get('/details/{id}', [DetailsController::class, 'details']);
			$this->router->get('/reservations', [BookingsController::class, 'bookings']);
			$this->router->post('/details/{id}', [DetailsController::class, 'newBooking']);
			$this->router->get('/login/{logout}', [AuthController::class, 'logout']);
		});
		$this->router->group(['middleware' => [OwnerMiddleware::class]], function () {


			$this->router->get('/nouvelle-annonce', [NewRentalController::class, 'newRental']);
			$this->router->post('/nouvelle-annonce', [NewRentalController::class, 'addRental']);
		});
	}

	private function startRouter(): void
	{
		try {
			$this->router->dispatch();
		} catch (RouteNotFoundException $e) {
			View::renderError();
		} catch (InvalidCallableException $e) {
			View::renderError(500);
		}
	}

	private function __clone()
	{
	}
	private function __wakeup()
	{
	}
}
