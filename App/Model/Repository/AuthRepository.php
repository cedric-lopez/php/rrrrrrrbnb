<?php

namespace App\Model\Repository;

use App\App;
use LidemCore\Repository;



class AuthRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'bookings';
	}


	public function addUser(): string
	{


		$data =
			[
				'email' => $_POST['email'],
				'pass' => hash('sha1', $_POST['pass']),
				'type' => isset($_POST['owner']) ? 2 : 1

			];
		$q = "INSERT INTO users (email,pass,type) VALUES (:email,:pass,:type)";
		$stmt = $this->pdo->prepare($q);

		$test = $stmt->execute($data);

		if ($test) {
			return 'compte créé';
		}
		return 'oups';
	}
	public function login()
	{
		$email = $_POST['email'];
		$pass = hash('sha1', $_POST['pass']);

		$q = sprintf('SELECT * FROM users WHERE email="%s" AND pass="%s" ', $email, $pass);

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		$sth->execute();

		$row_data = $sth->fetch();
		if (!empty($row_data)) {
			$_SESSION['id'] = $row_data['id'];
			$_SESSION['type'] = $row_data['type'] == 1 ? false : true;
			return $row_data['id'];
		}
		header('Location: /');
	}
}
