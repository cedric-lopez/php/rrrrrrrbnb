<?php

namespace App\Model\Repository;


use App\Model\Equipment;
use LidemCore\Repository;

class EquipmentRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'equipments';
	}

	public function findAll(): array
	{
		return $this->readAll(Equipment::class);
	}

	public function findByRental(int $id)
	{
		$arr_result = [];
		$q = sprintf('SELECT * FROM rental_equipment JOIN equipments ON rental_equipment.equipment_id = equipments.id WHERE rental_equipment.rental_id = :id  ');

		$sth = $this->pdo->prepare($q);

		if (!$sth) return $arr_result;

		$sth->execute(['id' => $id]);

		while ($row_data = $sth->fetch()) {
			$equipment = new Equipment($row_data);
			$arr_result[] = $equipment;
		}

		return $arr_result;
	}
	public function addEquipments($equipments, $id): void
	{


		$q = "INSERT INTO rental_equipment (rental_id,equipment_id) VALUES (:rental_id,:equipment_id)";
		$stmt = $this->pdo->prepare($q);
		foreach ($equipments as $equipment) {
			$stmt->execute(
				[
					'rental_id' => $id,
					'equipment_id' => $equipment
				]
			);
		}
	}
}
