<?php

namespace App\Model\Repository;


use App\Model\Rental;
use LidemCore\Repository;
use App\Model\Address;
use App\AppRepoManager;
use App\App;



class RentalRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'rentals';
	}




	public function findAll(): array
	{
		$arr_result = [];
		$q = sprintf('SELECT * FROM `%s` JOIN `%s` ON rentals.address_id = addresses.id', 'addresses', $this->getTableName());

		$sth = $this->pdo->query($q);

		if (!$sth) return $arr_result;

		while ($row_data = $sth->fetch()) {

			$rental = new Rental($row_data);
			$rental->address = new Address($row_data);
			$rental->address->id = $rental->address_id;
			$arr_result[] = $rental;
		}
		return $arr_result;
	}
	public function findAllByOwner(): array
	{
		$arr_result = [];

		$q = sprintf('SELECT * FROM `%s` JOIN `%s` ON rentals.address_id = addresses.id WHERE rentals.user_id = %s', 'addresses', $this->getTableName(), $_SESSION['id']);
		$sth = $this->pdo->query($q);

		if (!$sth) return $arr_result;

		while ($row_data = $sth->fetch()) {


			$rental = new Rental($row_data);
			$rental->address = new Address($row_data);
			$rental->address->id = $rental->address_id;
			$arr_result[] = $rental;
		}
		return $arr_result;
	}

	public function findById(int $id): ?Rental
	{
		$q = sprintf('SELECT * FROM `%s` JOIN `%s` WHERE rentals.id=:id', $this->getTableName(), 'addresses');

		$sth = $this->pdo->prepare($q);

		if (!$sth) return null;

		$sth->execute(['id' => $id]);

		$row_data = $sth->fetch();
		if (!empty($row_data)) {
			$rental = new Rental($row_data);
			$rental->address = new Address($row_data);
			$rental->address->id = $rental->address_id;
			$rental->equipments = AppRepoManager::getRm()->getEquipmentRepo()->findByRental($id);
			return $rental;
		}

		return null;
	}
	public function addRental($tmp_name, $name): string
	{



		$address = AppRepoManager::getRm()->getAddressRepo()->rentalAddress($_POST['country'], $_POST['city']);

		$data =
			[
				'surface' => $_POST['surface'],
				'title' => $_POST['title'],
				'description' => $_POST['title'],
				'capacity' => $_POST['capacity'],
				'price' => $_POST['price'],
				'type' => $_POST['type'],
				'address_id' => $address,
				'user_id' => $_SESSION['id'],
				'image' => $name
			];
		$q = "INSERT INTO rentals (surface,title,description,capacity,price,type,address_id,user_id,image) VALUES (:surface,:title,:description,:capacity,:price,:type,:address_id,:user_id,:image)";
		$stmt = $this->pdo->prepare($q);

		$test = $stmt->execute($data);

		if ($test) {
			$id = $this->pdo->lastInsertId();
			isset($_POST['equipments']) ? AppRepoManager::getRm()->getEquipmentRepo()->addEquipments($_POST['equipments'], $id) : '';
			move_uploaded_file($tmp_name, './assets/upload/' . $id . $name);
			return 'cool';
		}
		return 'oups';
	}
}
