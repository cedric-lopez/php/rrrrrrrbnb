<?php

namespace App\Model\Repository;


use App\Model\Address;
use LidemCore\Repository;

class AddressRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'addresses';
	}

	public function findAddress($country, $city): ?string
	{

		$q = sprintf('SELECT * FROM `%s` WHERE country="%s" AND city="%s" ', $this->getTableName(), $country, $city);

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;

		$sth->execute();

		$row_data = $sth->fetch();
		if (!empty($row_data)) {
			return $row_data['id'];
		}
		return null;
	}

	public function addAddress($country, $city): string
	{

		$data =
			[
				'country' => $country,
				'city' => $city,
			];
		$q = "INSERT INTO addresses (country,city) VALUES (:country, :city)";
		$stmt = $this->pdo->prepare($q);

		$test = $stmt->execute($data);
		$id = $this->pdo->lastInsertId();
		if ($test) return $id;

		return 'oups';
	}
	public function rentalAddress($country, $city): string
	{
		if (is_null($this->findAddress($country, $city))) {
			return $this->addAddress($country, $city);
		}
		return $this->findAddress($country, $city);
	}
}
