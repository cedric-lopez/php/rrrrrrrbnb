<?php

namespace App\Model\Repository;

use App\App;
use App\Model\Booking;
use App\Model\Rental;
use App\Model\Address;
use LidemCore\Repository;



class BookingsRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'bookings';
	}

	public function findAll(): array
	{
		$arr_result = [];
		$q = sprintf('SELECT 

		rentals.id,
		 rentals.title,
		 rentals.type,
		 rentals.user_id,
		 rentals.price,
		 rentals.capacity,
		 rentals.surface,
		 bookings.rental_id,
		 bookings.check_in,
		 bookings.check_out,
		 rentals.address_id,
		 addresses.id,
		 addresses.country,
		 addresses.city
		
		
		FROM `%s` JOIN `%s` ON rentals.id = bookings.rental_id JOIN addresses ON rentals.address_id = addresses.id WHERE bookings.user_id = %s', $this->getTableName(), 'rentals', $_SESSION['id']);
		$sth = $this->pdo->query($q);

		if (!$sth) return $arr_result;

		while ($row_data = $sth->fetch()) {

			$booking = new Booking($row_data);
			$booking->rental = new Rental($row_data);
			$booking->rental->id = $row_data['rental_id'];
			$booking->rental->address = new Address($row_data);
			$booking->rental->address->id = $booking->rental->address_id;
			$arr_result[] = $booking;
		}
		return $arr_result;
	}
	public function findAllByOwner(): array
	{
		$arr_result = [];
		$q = sprintf('SELECT 

		C.id,
		B.id,
		 B.title,
		 B.type,
		 B.user_id,
		 B.price,
		 B.capacity,
		 B.surface,
		 A.rental_id,
		 A.check_in,
		 A.check_out,
		 B.address_id,
		 B.id,
		 addresses.id,
		 addresses.country,
		 addresses.city
		 
		
		
		FROM `%s` AS A JOIN `%s` AS B ON B.id = A.rental_id JOIN users AS C ON C.id = B.user_id JOIN addresses ON addresses.id = B.address_id WHERE C.id = %s', $this->getTableName(), 'rentals', $_SESSION['id']);
		$sth = $this->pdo->query($q);

		if (!$sth) return $arr_result;

		while ($row_data = $sth->fetch()) {

			$booking = new Booking($row_data);
			$booking->rental = new Rental($row_data);
			$booking->rental->id = $row_data['rental_id'];
			$booking->rental->address = new Address($row_data);
			$booking->rental->address->id = $booking->rental->address_id;
			$arr_result[] = $booking;
		}
		return $arr_result;
	}
	public function addBooking($id): string
	{


		$data =
			[
				'rental_id' => $id,
				'user_id' => $_SESSION['id'],
				'check_in' => $_POST['check_in'],
				'check_out' => $_POST['check_out']

			];
		$q = "INSERT INTO bookings (rental_id,user_id,check_in,check_out) VALUES (:rental_id,:user_id,:check_in,:check_out)";
		$stmt = $this->pdo->prepare($q);

		$test = $stmt->execute($data);

		if ($test) {
			return 'Réservation enregistrée';
		}
		return 'oups';
	}
}
