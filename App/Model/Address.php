<?php

namespace App\Model;

use LidemCore\Model;

class Address extends Model
{
    public string $city;
    public string $country;
}
