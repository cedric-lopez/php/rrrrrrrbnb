<?php

namespace App\Model;

use LidemCore\Model;
use App\Model\Address;

class Rental extends Model
{
	const TYPE_ABRIS = 1;
	const TYPE_CAVERNE = 2;
	const TYPE_HUTTE = 3;

	public float $surface;
	public string $title;
	public string $description;
	public int $capacity;
	public float $price;
	public int $type;
	public int $address_id;
	public string $image;
	public ?Address $address = null;

	public function GetType(): string
	{
		switch ($this->type) {
			case $this::TYPE_ABRIS:
				return 'Abris';
				break;
			case $this::TYPE_CAVERNE:
				return 'Caverne';
				break;
			case $this::TYPE_HUTTE:
				return 'Hutte';
				break;
			default:
				return '';
		}
	}
}
