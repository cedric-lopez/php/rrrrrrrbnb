<?php

namespace App\Model;

use LidemCore\Model;


class Booking extends Model
{
    public string $check_in;
    public string $check_out;
    public ?Rental $rental;
}
