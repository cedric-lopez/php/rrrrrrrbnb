<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use \Closure;

class GuestMiddleware
{
    public function handle(ServerRequestInterface $request, Closure $next)
    {
        if (!empty($_SESSION['id'])) {

            header('Location: /');
        } else {

            return $next($request);
        }
    }
}
