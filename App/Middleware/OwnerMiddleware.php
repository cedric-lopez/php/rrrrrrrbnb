<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use \Closure;

class OwnerMiddleware
{
    public function handle(ServerRequestInterface $request, Closure $next)
    {
        if (isset($_SESSION['id']) && $_SESSION['type']) {
            return $next($request);
        } else {
            header('Location: /');
        }
    }
}
