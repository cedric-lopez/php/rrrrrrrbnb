<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use \Closure;

class UserMiddleware
{
    public function handle(ServerRequestInterface $request, Closure $next)
    {
        if (empty($_SESSION['id'])) {
            header('Location: /inscription');
        } else {
            return $next($request);
        }
    }
}
