<?php

namespace App;

use App\Model\Repository\RentalRepository;
use App\Model\Repository\EquipmentRepository;
use App\Model\Repository\BookingsRepository;
use App\Model\Repository\AddressRepository;
use App\Model\Repository\AuthRepository;


use LidemCore\RepositoryManagerTrait;

class AppRepoManager
{
	use RepositoryManagerTrait;

	private RentalRepository $rentalRepository;
	private EquipmentRepository $equipmentRepository;
	private BookingsRepository $bookingsRepository;
	private AddressRepository $addressRepository;
	private AuthRepository $authRepository;
	public function getRentalRepo(): RentalRepository
	{
		return $this->rentalRepository;
	}

	public function getEquipmentRepo(): EquipmentRepository
	{
		return $this->equipmentRepository;
	}
	public function getBookingsRepo(): BookingsRepository
	{
		return $this->bookingsRepository;
	}
	public function getAddressRepo(): AddressRepository
	{
		return $this->addressRepository;
	}
	public function getAuthRepo(): AuthRepository
	{
		return $this->authRepository;
	}


	protected function __construct()
	{
		$config = App::getApp();

		$this->rentalRepository = new RentalRepository($config);
		$this->equipmentRepository = new EquipmentRepository($config);
		$this->bookingsRepository = new BookingsRepository($config);
		$this->addressRepository = new AddressRepository($config);
		$this->authRepository = new AuthRepository($config);
	}
}
